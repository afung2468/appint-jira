#!/usr/bin/env perl
use strict;
BEGIN {
    $| = 1;
    use utf8::all;
}
use lib '/usr/local/src/appint/scripts';
use Mobiroo;
use constant { DEFAULT_NO => TRUE, DEFAULT_YES => FALSE };
use constant
 {
     #: Flictec registered our generic key with google 
     #: Original UUID on the next line: 18a342d2-300a-4d5d-bfb3-e9c83733fb6e 
     RELEASE_KEYSTORE_UUID => '18a342d2-300a-4d5d-bfb3-e9c83733fb6g',
      RELEASE_KEYSTORE_PATH => '/usr/local/src/appint/crypt/release.keystore',
       RELEASE_KEYSTORE_PASS => 'Upee8wee',
        RELEASE_KEYSTORE_ALIAS => 'mobdrmrk'
  };

our $opt_force = FALSE;
our $opt_logrotate = TRUE;
our $opt_ziplevel = 1;

GetOptions
 ( "help" => \$opt_help,
   "h" => \$opt_usage,
   "man" => \$opt_man,
   "q|quiet" => \$opt_quiet,
   "v|verbose+" => \$opt_verbose,
   "log=s" => \$opt_logfile,
   "f|force!" => \$opt_force,
   "logrotate!" => \$opt_logrotate,
   "z|ziplevel=i" => \$opt_ziplevel,
 )  ||  pod2usage(-verbose => 0);
pod2usage(-verbose => 2)  if ($opt_man);
pod2usage(-verbose => 1)  if ($opt_help);
pod2usage(-verbose => 0)  if ($opt_usage);
$opt_verbose = V_QUIET if ($opt_quiet);
$opt_logfile = undef if !$opt_logfile;

# ensure we have two arguments, UUID and path to apk
pod2usage(-verbose=>0) if @ARGV < 2;
our $CP_UUID = shift @ARGV;

# rotate logfiles
if (defined $opt_logfile && $opt_logrotate) {
    my $epoch = time();
    if (-f $opt_logfile) {
        my $counter = 0;
        my $tmp_logfile = $opt_logfile.".".$counter;
        while (-f $tmp_logfile) {
            $counter++;
            $tmp_logfile = $opt_logfile.".".$counter;
        }
        move($opt_logfile,$tmp_logfile);
        debug_msg("Rotated previous logfile to: ".$tmp_logfile);
    }
}

#
# Init
#

our %PORTAL =
 ( base => 'https://portal.mobileplatform.solutions',
   user => 'appint.portal.api',
   pass => 'ohf4IeGohno5oowuTaengae1Neiju8rohgh7Imier5ieji4aechau0aet9Yaixea',
 );

#
# Main Logic
#

notify_msg(">> Content Publisher: ".$CP_UUID);
notify_msg(">> Retrieving Content Publishers...");
our $CP_DATA = get_publishers();
if (exists $CP_DATA->{$CP_UUID}) {
    while (@ARGV) {
        my $APK_FILE = realpath(shift @ARGV);
        unless (-f $APK_FILE) {
            error_msg("Invalid path given: ".$APK_FILE);
            next;
        }
        notify_msg(">> Processing: ".$APK_FILE);
        do_jarsign($CP_UUID,$APK_FILE,$CP_DATA);
    }
} else {
    error_msg("CP Info Not Found.");
}

#
# End of Script
#
exit(RV_NORMAL);

#
# Sub-Functions
#

sub strip_signature {
    my ($path) = @_;
    my $cwd = getcwd();

    my $zipinfo_result = command('zipinfo','-1',$path);
    if ($zipinfo_result->{code}) {
        error_msg('Failed to zipinfo (not a valid ZIP archive?): '.$path);
        return FALSE;
    }
    # unless ($zipinfo_result->{output} =~ m!META\-INF!ms) {
    #     notify_msg('No META-INF found; skipping re-zip.');
    #     return TRUE;
    # }

    my (undef,$tmp_path) = tempfile('/tmp/mob-jarsigner-XXXXX',OPEN=>0);
    if (-d $tmp_path) {
        debug_msg("Removing existing tmp path: ".$tmp_path);
        remove_tree($tmp_path);
    }
    unless (mkdir($tmp_path)) {
        error_msg('Failed to make temp directory: '.$tmp_path);
        return FALSE;
    }
    unless (chdir($tmp_path)) {
        error_msg('Failed to change to temp directory: '.$tmp_path);
        return FALSE;
    }
    my $unzip_result = command('unzip',$path);
    if ($unzip_result->{code}) {
        error_msg('Failed to unzip '.$path);
        return FALSE;
    }
    remove_tree('META-INF') if -d 'META_INF';
    my $zip_result = command('zip','-r','-'.$opt_ziplevel,$path,'#*');
    if ($zip_result->{code}) {
        error_msg('Failed to zip '.$path);
        return FALSE;
    }
    chdir($cwd);
    return TRUE;
}

sub do_jarsign {
    my ($uuid,$path,$data) = @_;
#    unless (strip_signature($path)) {
#        error_msg("Failed to strip old signature. Skipping jarsigner process.");
#        return FALSE;
#    }

    if (lc($uuid) eq RELEASE_KEYSTORE_UUID) {
        # this is Flictec. They registered our generic signing key
        # with google and we went live with that. Fooy on us both lol
        my $sign_result =
         jarsign
          ( $path,
            RELEASE_KEYSTORE_PASS,
            RELEASE_KEYSTORE_PASS,
            RELEASE_KEYSTORE_PATH,
            RELEASE_KEYSTORE_ALIAS
          );
        if ($sign_result->{code}) {
            error_msg(">>>>>> Failed to jarsign [flictec] ".$path.", please check the logfile: ".$opt_logfile);
            return FALSE;
        }
        notify_msg(">>>>>> Sucessfully jarsigned [flictec] ".$path);
#        my $align_result = zipalign($path);
#        if ($align_result->{code}) {
#            error_msg(">>>>>> Failed to zipalign [flictec] ".$path.", please check the logfile: ".$opt_logfile);
#            return FALSE;
#        }
#        notify_msg(">>>>>> Sucessfully zipaligned [flictec] ".$path);
        return TRUE;
    } else {
        my (undef,$cp_keystore_path) = tempfile('/tmp/XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX',OPEN=>0);
        write_file($cp_keystore_path,decode_base64($data->{$uuid}{'Keystore-Data'}));
        my $keytool_result = keytool_list_v($data->{$uuid}{'Storepass-Decrypt'},$data->{$uuid}{'Keypass-Decrypt'},$cp_keystore_path);
        if ($keytool_result->{code}) {
            error_msg(YELLOW.'>>>>>> Skipping '.$uuid.' due to: invalid signing key details');
            unlink($cp_keystore_path) if -f $cp_keystore_path;
            return FALSE;
        }
        my ($cp_keystore_alias) = $keytool_result->{output} =~ m!^Alias name\:\s*(\w+)\s*$!ms;
        debug_msg("Keystore Information:\n".$keytool_result->{output});
        my $sign_result =
         jarsign
          ( $path,
            $data->{$uuid}{'Storepass-Decrypt'},
            $data->{$uuid}{'Keypass-Decrypt'},
            $cp_keystore_path,
            $cp_keystore_alias
          );
        if ($sign_result->{code}) {
            error_msg(">>>>>> Failed to jarsign ".$path.", please check the logfile: ".$opt_logfile);
            unlink($cp_keystore_path) if -f $cp_keystore_path;
            return FALSE;
        }
        unlink($cp_keystore_path);
        notify_msg(">>>>>> Sucessfully jarsigned ".$path);
#        my $align_result = zipalign($path);
#        if ($align_result->{code}) {
#            error_msg(">>>>>> Failed to zipalign ".$path.", please check the logfile: ".$opt_logfile);
#            unlink($cp_keystore_path) if -f $cp_keystore_path;
#            return FALSE;
#        }
#        notify_msg(">>>>>> Sucessfully zipaligned ".$path);
        unlink($cp_keystore_path) if -f $cp_keystore_path;
        return TRUE;
    }
}

sub get_publishers {
    my $login_res = ng_login($PORTAL{base},$PORTAL{user},$PORTAL{pass});
    my $raw_xml = ng_get_appint('appint/skeys');
    my %data = ();
    my @publishers_blocks = $raw_xml =~ m!<publisher>(.+?)</publisher>!msg;
    foreach my $publisher_block (@publishers_blocks) {
        my %block_data = $publisher_block =~ m!<\s*([^>]+?)\s*>(.+?)</\s*\1\s*>!msg;
        my $uuid = $block_data{'Node-UUID'};
        $data{ $uuid } = \%block_data;
    }
    return \%data;
}

__END__

=head1 NAME

 mob-jarsigner - Jarsign, zipalign the given APK with the given Content Publisher's signing key.

=head1 SYNOPSIS

 mob-jarsigner [-h|--help|--man] [-v|--verbose] [-q|--quiet] [--log=<path>] [-z|--zip-level=<INT>] <UUID> </path/to/apk> [/path/to/apks ...]

=head1 OPTIONS

=over 8

=item B<-h>

Prints a brief usage blurb.

=item B<--help>

Print the detailed help screen.

=item B<--man>

View the full documentation.

=item B<-v --verbose>

Increase verbosity.

=item B<-q --quiet>

Silence all output (except errors).

=item B<--log=path>

Specify alternate path for logging notices.

Default is as follows: /usr/local/src/appint/logs/mob-jarsigner.log

=item B<--no-logrotate>

By default, this script will rotate logfiles with each new run.
Previous logfiles get an incremental number appended to their filenames.
For example, if you run this script three times, the logfiles are renamed
as follows:

 #1 /usr/local/src/appint/logs/mob-jarsigner.log -> /usr/local/src/appint/logs/mob-jarsigner.log.0
 #2 /usr/local/src/appint/logs/mob-jarsigner.log -> /usr/local/src/appint/logs/mob-jarsigner.log.1
 #3 /usr/local/src/appint/logs/mob-jarsigner.log -> /usr/local/src/appint/logs/mob-jarsigner.log.2

If you specify B<--no-logrotate>, this script will append log messages if
the logfile already exists and it will create the logfile if it doesn't.

=item B<-z --zip-level=INT>

Specify the ZIP compression level to use.

=back

=head1 DESCRIPTION

This script, given a Content Publisher UUID and a list of APKs, will apply
the Content Publisher's signing keys to the files, one at a time.

=cut
