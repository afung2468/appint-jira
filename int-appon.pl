#!/usr/bin/perl

use strict;
use diagnostics;
use warnings;
use Getopt::Long;
use Pod::Usage;
use Data::Dumper;
# use lib '/home/afung/Documents/perl/JIRA-Client-Automated-1.2/lib';
use lib './JIRA-Client-Automated-1.2/lib';
# use Test::More;
use Term::ANSIColor;
use JIRA::Client::Automated;

BEGIN {
    $| = 1;                     # unbuffered
    # use_ok('JIRA::Client::Automated');
    # use utf8::all;
}

# my $jira_server   = 'https://jira.mobiroo.org';
# my $jira_server   = 'http://192.168.3.39:8080/';
my $jira_server   = 'https://mobiroo.atlassian.net/';
my $jira_project  = 'APPON';
my $jira_user     = 'afung';
# my $jira_password = 'apple0661';
my $jira_password = 'Fw10802#';

my $JCA = 'JIRA::Client::Automated';
my ($jira, $issue, $key, @issues, $link_types, $iss_number);
my (%issue_hash, @issue_keys, $issue_id, $issue_self, $issue_fields, $pkg_name);
my (%issue_fields_hash, @issue_fields_hash_keys, $cp_uuid, $app_uuid, $file_uuid, $appint_type, $target_channel);
my ($appint_type_ref, %appint_type_hash, $target_channel_ref, $target_channel_aref);
my ($narg, $issn, $cmdout, $conout, $cmdparms, $astring, $pkg, $sha, $pth);
my ($nosec, $zipzero, $type_len, $channel_len, $upd_sapps, $cmdout_len);
my ($opt_man, $opt_help, $opt_usage);

$nosec = "";
$zipzero = "";
GetOptions 
("help" => \$opt_help,
   "h" => \$opt_usage,
   "man" => \$opt_man,'no-secure' => \$nosec, 'zip-zero' => \$zipzero
   )  ||  pod2usage(-verbose => 0);
pod2usage(-verbose => 1)  if ($opt_man);
pod2usage(-verbose => 1)  if ($opt_help);
pod2usage(-verbose => 0)  if ($opt_usage);   

# Create new JCA object
# ok($jira = JIRA::Client::Automated->new($jira_server, $jira_user, $jira_password), 'new');
$jira = JIRA::Client::Automated->new($jira_server, $jira_user, $jira_password);
$upd_sapps = 0;
# isa_ok($jira, $JCA);

# save link_types for later testing
#ok($link_types = $jira->get_link_types(), 'get_link_types');
$link_types = $jira->get_link_types();
# cmp_ok( (grep { $_->{name} eq 'Blocks' } @$link_types), '>=', 1, 'has blocks link type');


if (grep { $_->{name} eq 'Blocks' } @$link_types) {
    print "Link has blocks link type \n";
}

if (@ARGV >0) {
  $narg = scalar @ARGV;
  print "Number of arguments: " . $narg . "\n\n";
} else {
  die "Must enter at least 1 APPON ticket number. \n";
} 

my $logfile = 'appon_log.txt';
open(my $fh, '>', $logfile) or die "Could not open log file '$logfile' $!";
open ($conout, '>&', STDOUT);

for ($issn = 0; $issn<$narg; $issn++) {
    my $appon_num = $ARGV[$issn];
    my $appon_ticket = 'APPON-' . $appon_num;
    $issue = $jira->get_issue($appon_ticket);
    # print Dumper($issue);

    %issue_hash = %$issue;
    @issue_keys = keys %issue_hash;
    $issue_id = $issue_hash{'id'};
    $issue_self = $issue_hash{'self'};
    $issue_fields = $issue_hash{'fields'};
    %issue_fields_hash = %$issue_fields;
    @issue_fields_hash_keys = keys %issue_fields_hash;
    $cp_uuid = $issue_fields_hash{'customfield_10042'};
    $cp_uuid =~ s~^\s+|\s+$~~g;   # remove any spaces
    $app_uuid = $issue_fields_hash{'customfield_10044'};
    $app_uuid =~ s~^\s+|\s+$~~g;   # remove any spaces
    $file_uuid = $issue_fields_hash{'customfield_10045'};
    $file_uuid =~ s~^\s+|\s+$~~g;   # remove any spaces
    $pkg_name = $issue_fields_hash{'customfield_10047'};
    # if ((length $pkg_name) > 2) {
    if (defined $pkg_name) {
        
        $pkg_name =~ s~^\s+|\s+$~~g;   # remove any spaces
    }
    $appint_type_ref = $issue_fields_hash{'customfield_10051'};
    $target_channel_ref = $issue_fields_hash{'customfield_10200'};
    if (defined $appint_type_ref) {
        %appint_type_hash = %$appint_type_ref;
        $appint_type = lc $appint_type_hash{'value'};
        $type_len = length $appint_type;
    } else {
        $appint_type = "";
        $type_len = 0;
    }
    if (defined $target_channel_ref) {
        $target_channel_aref = @$target_channel_ref[0];
        $target_channel = lc $target_channel_aref->{value};
        $channel_len = length $target_channel;
    } else {
        $target_channel = "";
        $channel_len = 0;
    }    
    # print "Issue ID: " . $issue_id . "\n";
    # print "Issue_self: " . $issue_self . "\n";
    # print "Ticket number: " . $appon_ticket . "\n";
    # print "cp_uuid: " . $cp_uuid . "\n";
    # print $fh "Ticket number: " . $appon_ticket . "\n";
    # print $fh "cp_uuid: " . $cp_uuid . "\n";
    
    $cmdparms = " --channel " . $target_channel . " --appint " . $appint_type . " " . $cp_uuid . "/" . $app_uuid . "/" . $file_uuid;
    if ($nosec) {
        $cmdparms = " --no-secure -z 1" . $cmdparms;
    } elsif ($zipzero) {   # nosec and zipzero cannot be used at the same time
        $cmdparms = " --no-secure -z 0" . $cmdparms;
    }
    if ((defined $pkg_name) && ((length $pkg_name) > 2) && (($appint_type eq "c") || ($appint_type eq "e"))) {
        $cmdparms = " --pkgname " . $pkg_name . $cmdparms;
    }
    
    print "Processing Ticket: " . $appon_ticket . "\n";
    print "mob-integrate-sapps" . $cmdparms . "\n";
    # print "Package name: " . $pkg_name . "\n";
    
    open STDOUT, '>&', $fh;
    print "Processing Ticket: " . $appon_ticket . "\n";
    
    if (($type_len == 1) && ($channel_len > 1)) {
        $cmdout = `yes | mob-integrate-sapps $cmdparms`;
        $cmdout =~ s~\e\[\d+m~~mg;   # remove any ESC sequences for coloring. Looks bad in log file.
        $cmdout_len = length $cmdout;
        print $cmdout . "\n";
        # if ($cmdout =~ m~.Yn.\s$~) {   # Check if it ended with [Yn]. If so, update vapps/sapps
        if (($cmdout_len < 300) && ($cmdout_len > 100) && ($upd_sapps == 0)) {    # If integration script finished early, update vapps/sapps
            $upd_sapps = 1;
            print "Path not found. Updating vapps & sapps update now. \n";
            open STDOUT, '>&', $conout;
            print "Executing mob-update-vapps \n";
            $cmdout = `mob-update-vapps`;
            print "Executing mob-update-sapps \n";
            $cmdout = `mob-update-sapps`;
            open STDOUT, '>&', $fh;
            $cmdout = `yes | mob-integrate-sapps $cmdparms`;
            $cmdout =~ s~\e\[\d+m~~mg;   # remove any ESC sequences for coloring. Looks bad in log file.
            print $cmdout . "\n";
        }
        print "--------------------------------------------------------------------------------------------------------------------\n";
        open STDOUT, '>&', $conout;
        # print "Length of cmdout: " . length $cmdout . "\n";
        # print "mob-integrate-sapps --channel " . $target_channel . " --appint " . $appint_type . " " . $cp_uuid . "/" . $app_uuid . "/" . $file_uuid . "\n\n";
    } else {
        $cmdout = "";
        print "Invalid integration type or channel. \n";  # print to log file
        open STDOUT, '>&', $conout;
        print "Invalid integration type or channel. \n";  # print to console
    }
    
    if($cmdout =~ m~\{noformat\}~) { 
        print "Integration succeeded! \n"; 
        $cmdout =~ m~(\{noformat\}.*\{noformat\})~s;   # Find the string between noformats
        $astring = $1;
        print $astring . "\n";
        if ($nosec) {
            $astring = "Integrated with \"--no-secure -z 1\".\n" . $astring;
        } elsif ($zipzero) {
            $astring = "Integrated with \"--no-secure -z 0\".\n" . $astring;    
        }    
        $astring =~ m~Pkg Name: (.*)~;
        $pkg = $1;
        $astring =~ m~SHA1 Sum: (.*)~;
        $sha = $1;
        $astring =~ m~WWW Path: (.*)~;
        $pth = $1;
        $jira->create_comment($appon_ticket, $astring);
        $jira->update_issue($appon_ticket, { customfield_10047 => $pkg });
        $jira->update_issue($appon_ticket, { customfield_10046 => $sha });
        $jira->update_issue($appon_ticket, { customfield_10031 => $pth });
 
    } else {
        print "Integration failed! \n";
    }
}

close $fh;
# done_testing();


__END__

=head1 NAME

 int-appon - Integrate 1 or more Jira APPON tickets.

=head1 SYNOPSIS

 int-appon [-h|--help|--man] [-n|--no-secure] [-z|--zipzero] <1 or more APPON ticket numbers separated by spaces>

=head1 OPTIONS

This script read the Jira APPON tickets specified, one at a time. It 
iterates through all the specified tickets and integrate them with
mob-integrate-sapps. If the directory of the sapp is not found, this
script will run mob-update-vapps and mob-update-sapps. 

For Types C and E apps, this script would get the package name from the 
Jira ticket and integrate with that package name if it is not blank. 

=over 8

=item B<-h>

Prints a brief usage blurb.

=item B<--help>

Print the detailed help screen.

=item B<--man>

View the full documentation.

=item B<--no-secure>

Do not dexprotect the integrated build. The default is to dexprotect.
This option forces the use of mob-jarsigner; specify the --no-secure 
variant. This option also use zip level 1.

=item B<--zipzero>

Set zip level to zero: that is no compression. This option also skips
dexprotect in the integrated build. 

=back

=cut